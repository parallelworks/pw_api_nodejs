var Client = require('./client');

// inputs
var pw_url = "https://eval.parallel.works";
var api_key = process.env['APIKEY'];
var workspace_name = "helyxos_workspace";
var inzip = "inputs.tgz";
var workflow_name = "helyxos_runner";
var command = "mesh_parallel";
var runpools = ["linuxpool"];

// create a new Parallel Works client
var c = Client(pw_url, api_key)

// check if the computing resources are running
function checkResourceStatus(p) {
    c.check_if_running(runpools[p]).then(function(poolstatus) {
        if (poolstatus == "on") {
            console.log(runpools[p] + " resource is running... proceeding with workflow");
        }
        else {
            console.log(runpools[p] + " resource is not running... please turn Parallel Works resource on");
            process.exit(1)
        }
        if (p < runpools.length - 1) {
            checkResourceStatus(p + 1);
        }
        else {
            getWorkspaceId();
        }
    });
}
if (runpools.length > 0) {
    checkResourceStatus(0);
}
else {
    getWorkspaceId();
}

// get workspace id from workspace name
function getWorkspaceId() {
    c.get_wid(workspace_name).then(function(wid) {
        c.wid = wid;
        uploadDataset();
    });
}

// upload the dataset(s)
function uploadDataset() {
    console.log("Uploading the Dataset");
    var did = c.upload_dataset(c.wid, inzip);
    did.then(function(did) {
        c.did = did;
        c.poll_upload(c.did).then(function() {
            executeCase();
        });
    });
}

// start the pw job
function executeCase(){
    console.log("Executing the Case");
    c.start_job(c.wid,c.did,workflow_name,command).then(function(jid){
        c.jid = jid;
        // poll job and wait for result id
        c.poll_job(c.jid).then(function(rid) {
            c.rid = rid;
            downloadResult();
        });
    });
}

// download result file
function downloadResult(){
    c.get_download_url(c.rid).then(function(durl){
      console.log("Workflow Execution Complete... Results Below:")  
      console.log(durl)  
    })
}
